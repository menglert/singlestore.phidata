## Use SingleStore as Vector Storage for AI Apps

> Note: Fork and clone this repository if needed

1. Create a virtual environment

```shell
python3 -m venv ~/.venvs/aienv
source ~/.venvs/aienv/bin/activate
```

2. Install libraries

```shell
pip install -r cookbook/singlestore/requirements.txt
```

3. Provide configuration information for SingleStore database credentials, LLM Chat Model and Embedding model

You can provide the configuration information in the following ways:

- Add the configuration information to the `resources.py` file

or

- Export the configuration information as environment variables

```shell
export SINGLESTORE_HOST="host"
export SINGLESTORE_PORT="3333"
export SINGLESTORE_USERNAME="user"
export SINGLESTORE_PASSWORD="password"
export SINGLESTORE_DATABASE="db"
export SINGLESTORE_SSL_CA=".certs/singlestore_bundle.pem"
export LLM="Claude/Anthropic/Groq"
export LLM_MODEL="claude-3-<haiku/opus/sonet>-20240307/..."
# Ollama has to be locally installed and a Embedding model has to be downloaded
export EMBEDDER="Ollama/OpenAI"
export EMBEDDING_MODEL="nomic-embed-text/text-embedding-ada-002/openhermes"
# Provide the LLM API Key e.g. one of those
export ANTHROPIC_API_KEY="api_key"
export GROQ_API_KEY="api_key"
export OPENAI_API_KEY="api_key"
```

4. Run AI as a CLI Application

```shell
python cookbook/singlestore/cli.py
```

5. Run Streamlit application

```shell
streamlit run cookbook/singlestore/app.py
```

- Open [localhost:8501](http://localhost:8501) to view your local AI app.
- Upload you own PDFs and ask questions

6. Message us on [discord](https://discord.gg/4MtYHHrgA8) if you have any questions
