from typing import List

import streamlit as st
from phi.assistant import Assistant
from phi.document import Document
from phi.document.reader.pdf import PDFReader
from phi.document.reader.website import WebsiteReader
from phi.tools.streamlit.components import (
    check_password,
    reload_button_sidebar,
    get_username_sidebar,
)

from assistant import get_s2_assistant  # type: ignore
from logging import getLogger

logger = getLogger(__name__)

st.set_page_config(
    page_title="SingleStoreAI",
    page_icon=":orange_heart:",
)
st.title("SingleStore AI")
st.markdown("##### :orange_heart: built using [phidata](https://github.com/phidatahq/phidata)")


def restart_assistant():
    st.session_state["s2_assistant"] = None
    st.session_state["s2_assistant_run_id"] = None
    st.session_state["file_uploader_key"] += 1
    st.session_state["url_scrape_key"] += 1
    st.rerun()


def main() -> None:
    # Get username
    username = get_username_sidebar()
    if username:
        st.sidebar.info(f":technologist: User: {username}")
    else:
        st.write(":technologist: Please enter a username")
        return

    # Get the assistant
    s2_assistant: Assistant
    if "s2_assistant" not in st.session_state or st.session_state["s2_assistant"] is None:
        logger.info("---*--- Creating Assistant ---*---")
        s2_assistant = get_s2_assistant(
            user_id=username,
            debug_mode=True,
        )
        st.session_state["s2_assistant"] = s2_assistant
    else:
        s2_assistant = st.session_state["s2_assistant"]

    # Create assistant run (i.e. log to database) and save run_id in session state
    try:
        st.session_state["s2_assistant_run_id"] = s2_assistant.create_run()
    except Exception:
        st.warning("Could not create assistant, is the database running?")
        return

    # Load existing messages
    assistant_chat_history = s2_assistant.memory.get_chat_history()
    if len(assistant_chat_history) > 0:
        logger.debug("Loading chat history")
        st.session_state["messages"] = assistant_chat_history
    else:
        logger.debug("No chat history found")
        st.session_state["messages"] = [{"role": "assistant", "content": "Ask me anything..."}]

    # Prompt for user input
    if prompt := st.chat_input():
        st.session_state["messages"].append({"role": "user", "content": prompt})

    # Display existing chat messages
    for message in st.session_state["messages"]:
        if message["role"] == "system":
            continue
        with st.chat_message(message["role"]):
            st.write(message["content"])

    # If last message is from a user, generate a new response
    last_message = st.session_state["messages"][-1]
    if last_message.get("role") == "user":
        question = last_message["content"]
        with st.chat_message("assistant"):
            response = ""
            resp_container = st.empty()
            for delta in s2_assistant.run(question):
                response += delta  # type: ignore
                resp_container.markdown(response)

            st.session_state["messages"].append({"role": "assistant", "content": response})

    if st.sidebar.button("New Run"):
        restart_assistant()

    if s2_assistant.knowledge_base and s2_assistant.knowledge_base.vector_db:
        if st.sidebar.button("Clear Knowledge Base"):
            s2_assistant.knowledge_base.vector_db.clear()
            st.session_state["knowledge_base_loaded"] = False
            st.sidebar.success("Knowledge base cleared")

    if st.sidebar.button("Auto Rename"):
        s2_assistant.auto_rename_run()

    # Upload Knowledge
    if s2_assistant.knowledge_base:
        if "file_uploader_key" not in st.session_state:
            st.session_state["file_uploader_key"] = 0

        uploaded_file = st.sidebar.file_uploader(
            "Upload PDF",
            type="pdf",
            key=st.session_state["file_uploader_key"],
        )        
        if uploaded_file is not None:
            alert = st.sidebar.info("Processing PDF...", icon="🧠")
            pdf_name = uploaded_file.name.split(".")[0]
            if f"{pdf_name}_uploaded" not in st.session_state:
                reader = PDFReader()
                documents: List[Document] = reader.read(uploaded_file)
                if documents:
                    s2_assistant.knowledge_base.load_documents(documents, upsert=True)
                else:
                    st.sidebar.error("Could not read PDF")
                st.session_state[f"{pdf_name}_uploaded"] = True
            alert.empty()
        
        if "url_scrape_key" not in st.session_state:
            st.session_state["url_scrape_key"] = 100

        scraped_url = st.sidebar.text_input(
            "Input URL",
            type="default",
            key=st.session_state["url_scrape_key"]
        )
        append_button = st.sidebar.button("Search URL")
        if append_button:
            if scraped_url is not None:
                alert = st.sidebar.info("Processing URLs...", icon="🧠")
                if f"{scraped_url}_scraped" not in st.session_state:
                    scraper = WebsiteReader()
                    web_documents: List[Document] = scraper.read(scraped_url)
                    if web_documents:
                        s2_assistant.knowledge_base.load_documents(web_documents, upsert=True, skip_existing=True)
                    else:
                        st.sidebar.error("Could not read Website")
                    st.session_state[f"{scraped_url}_uploaded"] = True
                alert.empty()

    if s2_assistant.storage:
        s2_assistant_run_ids: List[str] = s2_assistant.storage.get_all_run_ids(user_id=username)
        new_s2_assistant_run_id = st.sidebar.selectbox("Run ID", options=s2_assistant_run_ids)
        if st.session_state["s2_assistant_run_id"] != new_s2_assistant_run_id:
            logger.info(f"---*--- Loading run: {new_s2_assistant_run_id} ---*---")
            st.session_state["s2_assistant"] = get_s2_assistant(
                user_id=username,
                run_id=new_s2_assistant_run_id,
                debug_mode=True,
            )
            st.rerun()

    s2_assistant_run_name = s2_assistant.run_name
    if s2_assistant_run_name:
        st.sidebar.write(f":thread: {s2_assistant_run_name}")

    # Show reload button
    reload_button_sidebar()


if check_password():
    main()
