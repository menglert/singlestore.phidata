import sys
from os import getenv
from typing import Optional

from phi.assistant import Assistant
from phi.knowledge import AssistantKnowledge
from phi.storage.assistant.singlestore import S2AssistantStorage
from phi.vectordb.singlestore import S2VectorDb

from resources import config  # type: ignore

llm = getenv("LLM", config["llm"]).lower()
embedder = getenv("EMBEDDER", config["embedder"]).lower()
embedding_model = getenv("EMBEDDING_MODEL", config["embedding_model"]).lower()
llm_model = getenv("LLM_MODEL", config["llm_model"]).lower()

if llm == "openai" or llm == "chatgpt":
    from phi.llm.openai import OpenAIChat
    llm=OpenAIChat(model=llm_model)
elif llm == "claude" or llm == "anthropic":
    from phi.llm.anthropic import Claude
    llm=Claude(model=llm_model)
elif llm == "groq":
    from phi.llm.groq import Groq
    llm=Groq(model=llm_model)
else:
    sys.exit(10)

if embedder == "ollama":
    from phi.embedder.ollama import OllamaEmbedder
    embedder=OllamaEmbedder(model=embedding_model)
elif embedder == "openai" or embedder == "chatgpt":
    from phi.embedder.openai import OpenAIEmbedder
    embedder=OpenAIEmbedder(model=embedding_model)
else:
    sys.exit(20)

host = getenv("SINGLESTORE_HOST", config["host"])
port = getenv("SINGLESTORE_PORT", config["port"])
username = getenv("SINGLESTORE_USERNAME", config["username"])
password = getenv("SINGLESTORE_PASSWORD", config["password"])
database = getenv("SINGLESTORE_DATABASE", config["database"])
charset = getenv("SINGLESTORE_DATABASE_CHARSET", config["charset"])
ssl_ca = getenv("SINGLESTORE_SSL_CA", config["ssl_ca"])
ssl_verify = getenv("SINGLESTORE_SSL_VERIFY", config["ssl_verify"])

db_url = f"mysql+pymysql://{username}:{password}@{host}:{port}/{database}?ssl_ca={ssl_ca}&ssl_verify_cert={ssl_verify}&charset={charset}"

assistant_storage = S2AssistantStorage(table_name="s2_assistant", schema=database, db_url=db_url)

assistant_knowledge = AssistantKnowledge(
    vector_db=S2VectorDb(collection="documents", schema=database, db_url=db_url, embedder=embedder),
    num_documents=5,
)


def get_s2_assistant(
    user_id: Optional[str] = None,
    run_id: Optional[str] = None,
    debug_mode: bool = False,
) -> Assistant:
    return Assistant(
        name="s2_assistant",
        run_id=run_id,
        user_id=user_id,
        llm=llm,
        storage=assistant_storage,
        knowledge_base=assistant_knowledge,
        use_tools=True,
        show_tool_calls=True,
        # This setting adds the last 4 messages from the chat history to the API call
        add_chat_history_to_messages=True,
        add_references_to_prompt=True,
        num_history_messages=4,
        # This setting tells the LLM to format messages in markdown
        markdown=True,
        debug_mode=debug_mode,
        description="You are 'SingleStoreAI' designed to help users answer questions from a knowledge base of PDFs.",
    )
