config = {
    #SingleStore
    "host": "host",
    "port": 3333,
    "username": "user",
    "password": "password",
    "database": "db",
    "charset": "utf8mb4",
    "ssl_ca": ".certs/singlestore_bundle.pem",
    "ssl_verify": True,
    #LLM
    "llm": "OpenAI",
    "llm_model": "gpt-4-turbo",
    #Embedder
    "embedder": "Ollama",
    "embedding_model": "nomic-embed-text"
}
